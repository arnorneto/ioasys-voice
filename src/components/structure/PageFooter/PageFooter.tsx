import * as S from "./PageFooterstyles";
import LogoMin from "assets/images/logos/ioasys-logo-min.png";
import WebLink from "../WebLink";

const PageFooter = () => (
  <S.FooterWrapper>
    <S.FooterContent>
      <S.FooterSection>
        <S.Logo src={LogoMin} alt={"ioasys logo"} />
        <div>
          <p>ioasys.</p>
          <p>Todos os direitos reservados.</p>
        </div>
      </S.FooterSection>
      <S.FooterSection>
        <div>
          <h2>Junte-se a nós!</h2>
          <h1>+55 31 4141 5148</h1>
          <h2>contato@ioasys.com.br</h2>
        </div>
        <div>
          <p>
            <WebLink href="https://www.instagram.com/ioasys/">
              Instagram
            </WebLink>
            <strong> | </strong>
            <WebLink href="https://www.facebook.com/ioasys">Facebook</WebLink>
            <strong> | </strong>
            <WebLink href="https://www.linkedin.com/company/ioasys">
              LinkedIn
            </WebLink>
            <strong> | </strong>
            <WebLink href="https://medium.com/@ioasys">Medium</WebLink>
            <strong> | </strong>
            <WebLink href="https://www.youtube.com/c/ioasys">Youtube</WebLink>
          </p>
        </div>
      </S.FooterSection>
    </S.FooterContent>
  </S.FooterWrapper>
);

export default PageFooter;
