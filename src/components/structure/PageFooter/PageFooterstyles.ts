import styled, { css } from "styled-components";

export const FooterWrapper = styled.div`
  display: flex;
  width: 100vw;
  height: 410px;
`;

export const Logo = styled.img`
  width: 44px;
`;

export const FooterContent = styled.div`
  display: flex;
  width: 100%;
  padding: 80px 120px;
`;

export const FooterSection = styled.div`
  ${({ theme: { typography } }) => css`
    display: flex;
    width: 50%;
    flex-direction: column;
    justify-content: space-between;

    h1 {
      font-size: ${typography.sizes.xl};
    }
    h2 {
      font-weight: normal;
      font-size: ${typography.sizes.lg};
    }
    p {
      font-family: ${typography.family.secondary};
      font-size: ${typography.sizes.md};
    }
    strong{
      font-weight: 300;
      font-size: ${typography.sizes.lg};
    }
  `}
`;
