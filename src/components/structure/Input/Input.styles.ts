import styled, { css } from "styled-components";

export const Input = styled.input`
  ${({ theme: { spacing, typography, colors } }) => css`
    height: ${spacing.xl};
    width: 377px;
    margin-bottom: 40px;
    box-shadow: 0px 4px 20px rgba(37, 37, 37, 0.25);
    border-radius: 8px;
    border: none;
    outline: none;

    font-family: ${typography.family.secondary};
    font-size: ${typography.sizes.md};
    padding: 0 ${spacing.sm};
    color: ${colors.neutral.gray};
    text-align: center;
  `}
`;
