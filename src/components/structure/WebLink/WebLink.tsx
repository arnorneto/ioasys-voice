import * as S from "./WebLink.styles";

export type WebLinkProps = {
  href: string;
  children: string;
};

const WebLink = ({ href, children }: WebLinkProps) => {
  return (
    <S.Link href={href} target="_blank" rel="noreferrer">
      {children}
    </S.Link>
  );
};

export default WebLink;
