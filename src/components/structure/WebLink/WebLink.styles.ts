import styled, { css } from "styled-components";

export const Link = styled.a`
  ${({ theme: { colors } }) => css`
    color: ${colors.neutral.white};
  `}
`;
