import * as S from "./PageHeader.styles";
import LogoFull from "assets/images/logos/ioasys-logo-full.png";

const PageHeader = () => (
  <S.HeaderWrapper>
    <S.Logo src={LogoFull} alt={"ioasys logo"} />
  </S.HeaderWrapper>
);

export default PageHeader;
