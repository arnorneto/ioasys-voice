import styled from "styled-components";

export const HeaderWrapper = styled.div`
  display: flex;
  width: 100vw;
`;

export const Logo = styled.img`
  height: 32px;
  margin: 40px;
`;
