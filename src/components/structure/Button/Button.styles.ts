import styled, { css } from "styled-components";

export const Button = styled.button`
  ${({ theme: { spacing, colors} }) => css`
    height: ${spacing.xl};
    width: 205px;
    color: ${colors.neutral.white};
    background-color: #e00f63;
    border: none;

    font-weight: 700;
    font-size: 13px;
    letter-spacing: 0.29em;
  `}
`;
