import * as S from "./Button.styles";
import { WithChildren } from "interfaces/children";

const Button = ({ children }: WithChildren) => <S.Button>{children}</S.Button>;

export default Button;
