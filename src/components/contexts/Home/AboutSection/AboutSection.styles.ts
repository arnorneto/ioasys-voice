import styled, { css } from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-bottom: 110px;
`;

export const SeloImg = styled.img`
  position: relative;
  top: 72px;
  right: 68px;
  height: 190px;
  width: 190px;
  align-self: flex-end;
`;

export const Content = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 110px;
`;

export const ContentImg = styled.img`
  width: 486px;
  height: 486px;
`;

export const AboutText = styled.div`
  ${({ theme: { typography } }) => css`
    display: flex;
    padding-left: 140px;
    flex-direction: column;
    justify-content: center;

    h1 {
      font-family: ${typography.family.secondary};
      font-size: 80px;
      line-height: 90px;
      margin-bottom: 26px;
    }

    p {
      font-family: ${typography.family.secondary};
      font-weight: 500;
      padding-right: 80px;
    }

    strong {
      background-image: linear-gradient(
        90deg,
        rgba(226, 96, 198, 1) 0%,
        rgba(211, 101, 144, 1) 50%,
        rgba(226, 96, 198, 1) 100%
      );
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
  `}
`;
