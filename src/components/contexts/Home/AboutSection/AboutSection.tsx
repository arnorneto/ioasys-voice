import * as S from "./AboutSection.styles";
import selo from "assets/images/logos/selo.png";
import voiceLogo from "assets/images/logos/voice.png";

const AboutSection = () => (
  <S.Wrapper>
    <S.SeloImg src={selo} alt="drops de cultura" />
    <S.Content>
      <S.ContentImg src={voiceLogo} />
      <S.AboutText>
        <h1>
          O que é o <strong>ioasys</strong> voice?
        </h1>
        <p>
          Um podcast quinzenal sobre tecnologia, inovação, design e comunicação.
          No ioasys voices, nossos participantes comentam sobre suas
          experiências, trazendo conteúdos inovadores e diferentes
          pontos-de-vista sobre cada tema.
        </p>
      </S.AboutText>
    </S.Content>
  </S.Wrapper>
);

export default AboutSection;
