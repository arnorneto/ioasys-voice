import * as S from "./SubscribeBanner.styles";
import Input from "components/structure/Input";
import Button from "components/structure/Button";

const SubscribeBanner = () => {
  return (
    <S.Wrapper>
      <S.Content>
        <h1>Quer receber o próximo podcast?</h1>
        <p>Digite seu e-mail e receba o material dos podcasts</p>
        <Input />
        <Button>ENVIAR</Button>
      </S.Content>
    </S.Wrapper>
  );
};

export default SubscribeBanner;
