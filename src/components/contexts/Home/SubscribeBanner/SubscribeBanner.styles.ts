import styled, { css } from "styled-components";
import BannerBackground from "assets/images/backgrounds/subscribe-bg.png";

export const Wrapper = styled.div`
  display: flex;
  height: 542px;
  justify-content: center;
  align-items: center;
  background-image: url(${BannerBackground});
  background-size: cover;
`;

export const Content = styled.div`
  ${({ theme: { colors, typography, spacing } }) => css`
    display: flex;
    width: 60%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;

    h1 {
      font-size: 60px;
      margin-bottom: ${spacing.xs};
      line-height: 70px;

      background-image: linear-gradient(
        135deg,
        rgba(126, 121, 108, 1) 18%,
        rgba(0, 0, 0, 1) 50%,
        rgba(35, 69, 198, 1) 60%,
        rgba(56, 195, 207, 1) 72%
      );
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }

    p {
      font-family: ${typography.family.secondary};
      color: ${colors.neutral.gray};
      margin-bottom: ${spacing.xs};
      line-height: 50px;
    }
  `}
`;
