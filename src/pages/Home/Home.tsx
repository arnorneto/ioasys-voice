import * as S from "./Home.styles";
import PageHeader from "components/structure/PageHeader";
import AboutSection from "components/contexts/Home/AboutSection";
import SubscribeBanner from "components/contexts/Home/SubscribeBanner";
import PageFooter from "components/structure/PageFooter";

const Home = () => {
  return (
    <S.Wrapper>
      <PageHeader />
      <AboutSection />
      <SubscribeBanner />
      <PageFooter />
    </S.Wrapper>
  );
};

export default Home;
