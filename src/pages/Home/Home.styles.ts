import styled, { css } from "styled-components";

export const Wrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    width: 100vw;
    min-height: 100vh;
    flex-direction: column;
    background: ${theme.colors.neutral.black};
  `}
`;
