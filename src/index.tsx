import React from "react";
import ReactDOM from "react-dom";
import Routes from "./routes";
import reportWebVitals from "./reportWebVitals";
import { AppContexts } from "./app.contexts";
import "assets/fonts/Gilroy/index.css";
import "assets/fonts/Heebo/index.css";

ReactDOM.render(
  <React.StrictMode>
    <AppContexts>
      <Routes />
    </AppContexts>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
